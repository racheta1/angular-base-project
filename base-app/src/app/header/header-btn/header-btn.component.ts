import { Component, Input, EventEmitter, Output } from '@angular/core';

@Component({
  selector: 'app-header-btn',
  templateUrl: './header-btn.component.html',
  styleUrls: ['./header-btn.component.scss']
})
export class HeaderBtnComponent{

  constructor() { }

  ngOnInit(): void {
  }

  @Output() hideHeaderParent = new EventEmitter();
  Hide = false;

  handleClick() {
    this.Hide = true;
    this.hideHeaderParent.emit(this.Hide);
}

}

import { Component, OnInit } from '@angular/core';
import { MatTableDataSource } from '@angular/material/table';
import { ImageService } from '../entities/image.service';
import { Image } from '../entities/image';
import { AuthGuard } from '../services/auth.guard';
import { Router } from '@angular/router';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss'],
})
export class TableComponent implements OnInit {
  dataSource!: MatTableDataSource<Image>;
  displayedColumns: string[] = [
    'checked',
    'name',
    'size',
    'recognition',
    'download',
  ];

  constructor(
    private imageService: ImageService,
    private authGuard: AuthGuard,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.authGuard.verifyAuth();
    this.getImagesList();
  }

  private getImagesList(): void {
    this.imageService.getImages().subscribe((list: Image[]) => {
      if (list.length < 0) {
        // this.router.navigate(['link_error/204'])
        console.log('orice mesaj vrei tu');
      } else {
        console.log(list);
        this.dataSource = new MatTableDataSource<Image>(list);
      }
    });
  }
}

export class Cloud {
  id: number | undefined;
  data: string | undefined;
  check: boolean | undefined;
}
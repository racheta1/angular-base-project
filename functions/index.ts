const {Datastore} = require('@google-cloud/datastore');
const datastore = new Datastore({
	projectId: 'solid-ego-310906',
	keyFilename: 'datastore-credential.json'
});
const kindName = 'log-message';

const cors = require('cors')({origin: false});

export async function getLogMessages(req : any, res : any) {
  cors(req, res, async () => {
	  
	res.set('Access-Control-Allow-Origin', '*');
	if (req.method === 'OPTIONS') {
    // Send response to OPTIONS requests
		res.set('Access-Control-Allow-Methods', 'GET');
		res.set('Access-Control-Allow-Headers', 'Authorization');
		res.set('Access-Control-Max-Age', '3600');
		res.status(204).send('');
	} else {

		const query = datastore.createQuery('log-message');
	
		let [logs] = await datastore.runQuery(query);
	
		res.send(logs);
	}
  })
  
};


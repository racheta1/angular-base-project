import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Image} from './image';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class ImageService {
  constructor(private http: HttpClient) {
  }

  getImages(): Observable<Image[]> {
    const url = 'http://localhost:3000/images'
    return this.http.get(url) as Observable<Image[]>;
  }
}

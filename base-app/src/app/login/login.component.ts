import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {UserService} from '../services/user.service';
import {HeaderService} from '../services/header.service';
import { AuthService } from '../services/auth.service';
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})

export class LoginComponent implements OnInit {
  public username: string="";
  public password: string="";
  public error: string="";
  
  userForm = new FormGroup({
    email: new FormControl('', [Validators.required, Validators.minLength(3), Validators.email]),
    psw: new FormControl('', [Validators.required, Validators.minLength(8)])
  });
  submitted = true;

  constructor(private router: Router, private fb: FormBuilder, private userService: UserService, private headerState: HeaderService, private auth: AuthService) {

  }


  public hasError = (email: string, errorName: string) => {
    return this.userForm.controls[email].hasError(errorName);
  };

  validate() {
    if (this.userForm.valid) {
      this.userService.login(this.userForm.getRawValue()).subscribe(data => {
        this.router.navigate(['/history']);
      }, error => {
      });
    }
  }

  onFormSubmit() {
    this.submitted = true;
    // if (this.userForm.invalid) {
    //   return;
    // }
    let body = {
      username: this.userForm.value.email,
      password: this.userForm.value.psw
    };
    console.log(this.userForm.value.email)

    this.auth.login(this.userForm.value.email, this.userForm.value.psw)
      .pipe(first())
      .subscribe(
        result => this.router.navigate(['history']),
        err => this.error = 'Could not authenticate'
      );

    if (this.userForm.valid) {
      this.router.navigate(['history']);
    }
  };

  ngOnInit(): void {

  }


}

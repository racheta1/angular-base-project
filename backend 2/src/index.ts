import "reflect-metadata";
import {Request, Response} from "express";
import {Routes} from "./routes";
import { nextTick } from "process";

    // create express app
    const _ = require('lodash');
    const express = require('express')
    const cors = require('cors')
    const app = express()
    const bodyParser = require('body-parser');
    const jwt = require('jsonwebtoken');
    const expressJwt = require('express-jwt');
    app.use(cors());
    app.use(bodyParser.json());
    // app.use(expressJwt({secret: 'secret', algorithms: ['RS256']}).unless({path: ['/api/auth']}));

    // register express routes from defined application routes
    Routes.forEach(route => {
        (app as any)[route.method](route.route, (req: Request, res: Response, next: Function) => {
            const result = (new (route.controller as any))[route.action](req, res, next);
            if (result instanceof Promise) {
                result.then(result => result !== null && result !== undefined ? res.send(result) : undefined);

            } else if (result !== null && result !== undefined) {
                res.json(result);
            }
        });
    });

    var TODOS = [
        { 'id': 1, 'user_id': 1, 'name': "name",  'size': 2,  'recognition': 2,  'download': "---"},
        { 'id': 2, 'user_id': 1, 'name': "name",  'size': 2,  'recognition': 2,  'download': "---"},
        { 'id': 3, 'user_id': 1, 'name': "name",  'size': 2,  'recognition': 2,  'download': "---"},
        { 'id': 4, 'user_id': 1, 'name': "name",  'size': 2,  'recognition': 2,  'download': "---"}
    ];
    var USERS = [
        { 'id': 1, 'email': "go@brr" }
    ];
    function getTodos(userID: any) {
        var todos = _.filter(TODOS, ['user_id', userID]);
    
        return todos;
    }
    function getTodo(todoID: any) {
        var todo = _.find(TODOS, function (todo: { id: any; }) { return todo.id == todoID; })
    
        return todo;
    }
    function getUsers() {
        return USERS;
    }
    
    app.get('/api/todos', function (req: { user: { userID: any; }; }, res: { type: (arg0: string) => void; send: (arg0: any) => void; }) {
        res.type("json");
        res.send(getTodos(req.user.userID));
    });
    app.get('/api/todos/:id', function (req: { params: { id: any; }; }, res: { type: (arg0: string) => void; send: (arg0: any) => void; }) {
        var todoID = req.params.id;
        res.type("json");
        res.send(getTodo(todoID));
    });
    app.get('/api/users', function (req: Request, res: Response) {
        res.type("json");
        res.send(getUsers());
    });
    
    app.listen(3000, function () {
        console.log('Angular JWT Todo API Server listening on port 3000!')
    });

    app.get('/', function (req: any, res: { send: (arg0: string) => void; }) {
        res.send('Angular JWT Todo API Server')
        console.log("working?")
    });
    app.post('/api/auth', function(req: Request, res: Response) {
        const body = req.body;
    
        const user = USERS.find(user => user.email == body.username);

        if(!user) return res.sendStatus(401); //|| body.password != '12345678'
        
        var token = jwt.sign({userID: 1}, 'secret', {expiresIn: '2h'}); //userID: user!.id
        res.send({token});
    });

    function checkJwt(req: Request, res: Response, next: () => void) {
        try {
            jwt.verify(req.headers.authorization, 'secret');
            next();
        }
        catch {

        }
    }

    // setup express app here
    // ...

    console.log("Express server has started on port 3000. Open http://localhost:3000/users to see results");


import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Cloud } from './cloud';

@Injectable({
  providedIn: 'root'
})
export class CloudService {
  constructor(private http: HttpClient) { }

  getCloudAll() {
    return this.http.get<Cloud[]>('/api/cloudAll');
  }

  getCloud(id: number) {
    return this.http.get<Cloud>(`/api/cloud/${id}`);
  }
}
import {Component, OnInit} from '@angular/core';
import { FileUploadService } from '../services/file-upload.service';

@Component({
  selector: 'upload-root',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.scss']
})

export class UploadComponent {

  url: any;
  msg = '';
  fileToUpload: File | null | undefined;

  constructor(private fileUpload: FileUploadService) {
    
  }

  selectFile(files: FileList) {
    if (!files[0] || files.length == 0) {
      this.msg = 'You must select an image';
      return;
    }

    var mimeType = files[0].type;

    if (mimeType.match(/image\/*/) == null) {
      this.msg = 'Only images are supported';
      return;
    }

    var reader = new FileReader();
    reader.readAsDataURL(files[0]);

    reader.onload = (_event) => {
      this.msg = '';
      this.url = reader.result;

      this.fileToUpload = files.item(0);
      console.log(files.item(0));
      this.uploadFileToActivity();
    };
  }

  uploadFileToActivity() {
    this.fileUpload.postFile(this.fileToUpload!).subscribe(data => {
       console.log("file uploaded. yay...")
      }, error => {
        console.log(error);
      });
  }

}


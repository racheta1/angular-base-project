import { LoginController } from "./controller/LoginController";

export const Routes = [{
    method: "post",
    route: "/login",
    controller: LoginController,
    action: "login"

}];
import "reflect-metadata";
import {createConnection} from "typeorm";
import * as express from "express";
import * as bodyParser from "body-parser";
import {Request, Response} from "express";
import {Routes} from "./routes";
import {User} from "./entity/User";
import {Image} from "./entity/Image";
require('@tensorflow/tfjs-node')


createConnection().then(async connection => {

    // create express app
    const express = require('express')
    const cors = require('cors')
    const app = express()
    const bodyParser = require('body-parser');
    const jwt = require('jsonwebtoken');
    const expressJwt = require('express-jwt');
    const userController = require('./controller/UserController')
    app.use(cors());
    app.use(bodyParser.json());


    // register express routes from defined application routes
    Routes.forEach(route => {
        (app as any)[route.method](route.route, (req: Request, res: Response, next: Function) => {
            const result = (new (route.controller as any))[route.action](req, res, next);
            if (result instanceof Promise) {
                result.then(result => result !== null && result !== undefined ? res.send(result) : undefined);

            } else if (result !== null && result !== undefined) {
                res.json(result);
            }
        });
    });

    const fileUpload = require('express-fileupload')
    app.use(fileUpload({
        useTempFiles : false,
        tempFileDir : '/tmp/'
    }));


    // setup express app here
    // ...
    app.post('/api/auth', async function(req: Request, res: Response) {
        const body = req.body;

        let users = await connection.manager.find(User);
        const user = users.find(user => user.firstName == body.username);

        if(!user) return res.sendStatus(401); //|| body.password != '12345678'
        
        var token = jwt.sign({userID: 1}, 'secret', {expiresIn: '2h'}); //userID: user!.id
        res.send({token});
    });

    function checkJwt(req: Request, res: Response, next: () => void) {
        try {
            jwt.verify(req.headers.authorization, 'secret');
            next();
        }
        catch {

        }
    }

    // start express server
    app.listen(3000);

    // insert new users for test
    await connection.manager.save(connection.manager.create(User, {
        firstName: "go@brr",
        lastName: "Saw",
        age: 27
    }));
    
    await connection.manager.save(connection.manager.create(User, {
        firstName: "Phantom",
        lastName: "Assassin",
        age: 24
    }));
      
    await connection.manager.save(connection.manager.create(Image, {
        checked: false, 
        name: 'Flower', 
        size: 240000, 
        recognition: 6, 
        download: '/assets/imgs/ghiocel.jpg'
    }));

    await connection.manager.save(connection.manager.create(Image, {
        checked: false,
        name: 'Power',
        size: 2370,
        recognition: 9,
        download: '/assets/imgs/ghiocel.jpg'
    }));

    await connection.manager.save(connection.manager.create(Image, {
        checked: false,
        name: 'Pwerpuff',
        size: 2602000,
        recognition: 16,
        download: '/assets/imgs/ghiocel.jpg'
    }));

    await connection.manager.save(connection.manager.create(Image, {
        checked: false,
        name: 'Rocket', 
        size: 3050, 
        recognition: 4, 
        download: '/assets/imgs/ghiocel.jpg'
    }));

    console.log("Express server has started on port 3000. Open http://localhost:3000/users to see results");

}).catch(error => console.log(error));

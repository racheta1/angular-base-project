const {Datastore} = require('@google-cloud/datastore');
const datastore = new Datastore({
	projectId: 'solid-ego-310906',
	keyFilename: 'datastore-credential.json'
});
const kindName = 'log-message';

exports.savelog = (req, res) => {
	let log_message = req.query.log_message || req.body.log_message || '';

	datastore
		.save({
			key: datastore.key(kindName),
			data: {
				log_message: log_message
			}
		})
		.catch(err => {
		    console.error('ERROR:', err);
		    res.status(200).send(err);
		    return;
		});

	res.status(200).send(log_message);
};
export class Image {
    checked: boolean | undefined;
    name: string | undefined;
    size: number | undefined;
    recognition: number | undefined;
    download: string | undefined;
  }
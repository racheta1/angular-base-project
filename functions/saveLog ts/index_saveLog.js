var {Datastore} = require('@google-cloud/datastore');
var datastore = new Datastore({
    projectId: 'solid-ego-310906',
    keyFilename: 'datastore-credential.json'
});
var kindName = 'log-message';
exports.savelog = function (req, res) {
    var log_message = req.query.log_message || req.body.log_message || '';
    datastore
        .save({
        key: datastore.key(kindName),
        data: {
            log_message: log_message
        }
    })["catch"](function (err) {
        console.error('ERROR:', err);
        res.status(200).send(err);
        return;
    });
    res.status(200).send(log_message);
};

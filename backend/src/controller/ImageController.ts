import {getRepository} from "typeorm";
import {NextFunction, Request, Response} from "express";
import {Image} from "../entity/Image";
import {MnistData} from '../data/data';
import {doPrediction, train, loadModel} from '../data/script';

const tf = require('@tensorflow/tfjs');
require('@tensorflow/tfjs-node');

export class ImageController {

    private imageRepository = getRepository(Image);

    async all(request: Request, response: Response, next: NextFunction) {
        return this.imageRepository.find();
    }

    async evaluate(request: Request, response: Response, next: NextFunction) {
        //load data
        const data = new MnistData();
        await data.load(request.files.img.data);
        //load model
        const model = await tf.loadLayersModel('file://src/assets/models/model.json');
        console.log(model.summary())

        //do prediction
        const [preds, labels] = doPrediction(model, data, 1);
        return [preds, labels];
    }
    async train(request: Request, response: Response, next: NextFunction) {
        //load train data
        const data = new MnistData();
        await data.load(request.files.img.data);
        //load model
        const model = loadModel();
        //train model
        const trainedModel =  train(model, data);
        //save model
        await model.save('file://src/assets/models/my_model');
        //return results
        return trainedModel;
    }

}

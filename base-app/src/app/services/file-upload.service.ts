import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class FileUploadService {

  constructor(private http: HttpClient) { }

  postFile(fileToUpload: File): Observable<boolean> {
    const endpoint = 'http://localhost:3000/api/TODO';
    const formData: FormData = new FormData();
    formData.append('fileKey', fileToUpload, fileToUpload.name);
    let headers = new HttpHeaders({
      // 'Content-Type': 'application/json',
      // 'Authorization': 'Bearer ' + fileToUpload
    })
    let options = { headers: headers }

    return this.http
      .post(endpoint, formData, options)
      .pipe(
        map(result => {
          return true;
        })
      );
}
}

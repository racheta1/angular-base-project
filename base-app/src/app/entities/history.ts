export class History {
  id: number | undefined;
  user_id: number | undefined;
  name: string | undefined;
  completed: boolean | undefined;
}
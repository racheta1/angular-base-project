import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { History } from '../entities/history'
import { HistoryService }from '../entities/history.service'

@Component({
  selector: 'app-history-list',
  templateUrl: './history-list.component.html',
  styleUrls: ['./history-list.component.scss']
})
export class HistoryListComponent implements OnInit {
  history$: Observable< History[]> | undefined;

  constructor(private histories: HistoryService) { }

  ngOnInit() {
    this.history$ = this.histories.getHistories();
  }
}
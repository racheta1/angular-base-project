import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {LoginComponent} from './login/login.component';
import {UploadComponent} from './upload/upload.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { JwtModule } from '@auth0/angular-jwt';

import {HttpClientModule} from '@angular/common/http';
import {RouterModule, Routes} from '@angular/router';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {TableComponent} from './table/table.component';
import {MatTableModule} from '@angular/material/table';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {FormsModule} from '@angular/forms';
import {MatInputModule} from '@angular/material/input';
import {MatButtonModule} from '@angular/material/button';
import {MatIconModule} from '@angular/material/icon';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatCardModule} from '@angular/material/card';
import {MatToolbarModule} from '@angular/material/toolbar';
import { HeaderComponent } from './header/header.component';
import { ReactiveFormsModule } from '@angular/forms';
import {MatFormFieldModule} from '@angular/material/form-field';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {UserService} from './services/user.service';
import {FileSizePipe} from './table/file-size.pipe';
import { HeaderBtnComponent } from './header/header-btn/header-btn.component';
import { AuthService } from './services/auth.service';
import { AuthGuard } from './services/auth.guard';
import { HistoryListComponent } from './history-list/history-list.component';
import { UserListComponent } from './user-list/user-list.component';
import { GoogleCloudComponent } from './google-cloud/google-cloud.component';

const routes: Routes = [
  {path: 'home', component: LoginComponent},
  {path: 'history', component: TableComponent},
  {path: 'upload', component: UploadComponent},
  {path: '', redirectTo: '/home', pathMatch: 'full'}];

  export function tokenGetter() {
    return localStorage.getItem('access_token');
  }

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    TableComponent,
    UploadComponent,
    HeaderComponent,
    FileSizePipe,
    HeaderBtnComponent,
    HistoryListComponent,
    UserListComponent,
    GoogleCloudComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    RouterModule.forRoot(routes),
    MatTableModule,
    MatCheckboxModule,
    FormsModule,
    MatButtonModule,
    MatToolbarModule,
    MatIconModule,
    MatInputModule,
    MatSidenavModule,
    MatCardModule,
    FlexLayoutModule,
    HttpClientModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    JwtModule.forRoot({
      config: {
        tokenGetter: tokenGetter,
        allowedDomains: ['localhost:3000'],
        disallowedRoutes: ['localhost:3000/api/auth']
      }
    })
  ],
  providers: [UserService,
    AuthService,
    AuthGuard],
  bootstrap: [AppComponent]
})

export class AppModule {
}


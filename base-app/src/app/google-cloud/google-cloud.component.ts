import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-google-cloud',
  templateUrl: './google-cloud.component.html',
  styleUrls: ['./google-cloud.component.scss']
})

export class GoogleCloudComponent implements OnInit {

  constructor(private  http: HttpClient) { }

  logs: string[] = []
  
  ngOnInit(): void {
    this.get().subscribe((data) =>{
      //this.Logs = JSON.stringify(data);
      
      for (let index = 0; index < data.length; index++) {
        const element = data[index]['log_message'];
        this.logs.push(element);
      }

      console.log(this.logs)
      //console.log(data)
    } );
  }

  public get() : Observable<any> {
    return this.http.get("https://us-central1-solid-ego-310906.cloudfunctions.net/getAllMessages");
  }

}

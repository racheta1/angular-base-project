import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {HeaderService} from '../services/header.service';
import {Observable, of} from 'rxjs';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  isLoggedIn$: Observable<boolean>;

  constructor(private headerService: HeaderService) {
    this.isLoggedIn$ = of(false);
  }

  ngOnInit(): void {
    this.isLoggedIn$ = this.headerService.isLoggedIn;
    console.log(this.headerService.isLoggedIn);
  }

  onLogout() {

  }

}

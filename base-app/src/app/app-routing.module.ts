import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LoginComponent } from './login/login.component';
import { AuthGuard } from './services//auth.guard';
import { HistoryListComponent } from './history-list/history-list.component';
import { UserListComponent } from './user-list/user-list.component';
import { GoogleCloudComponent } from './google-cloud/google-cloud.component';

const routes: Routes = [
  { path: 'histories', component: HistoryListComponent, canActivate: [AuthGuard] },
  { path: 'users', component: UserListComponent, canActivate: [AuthGuard] },
  { path: 'login', component: LoginComponent},
  { path: 'cloud', component: GoogleCloudComponent}]

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

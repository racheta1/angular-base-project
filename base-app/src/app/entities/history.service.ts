import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { History } from './history';

@Injectable({
  providedIn: 'root'
})
export class HistoryService {
  constructor(private http: HttpClient) { }

  getHistories() {
    return this.http.get<History[]>('/api/histories');
  }

  getHistory(id: number) {
    return this.http.get<History>(`/api/histories/${id}`);
  }
}
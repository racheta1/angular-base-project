import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from './services/auth.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  constructor(public auth: AuthService, private router: Router) { }

  logout() {
    this.auth.logout();
    this.router.navigate(['login']);
  }
  
  //copy me in header component after fixing it ->
  Display: string = "";

 hideHeader(hide: any) {
   if(hide) {
     this.Display = "none";
   }
}
 // <-
}

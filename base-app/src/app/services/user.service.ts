import {Injectable} from '@angular/core';
import {tap} from 'rxjs/operators';
import {User} from '../login/user';
import {RequestService} from './request.service';
import {HeaderService} from './header.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private userInfo: { password: string; username: string };

  constructor(private request: RequestService, private header: HeaderService) {
    this.userInfo = {username: '', password: ''};
  }

  // tslint:disable-next-line:typedef
  login(data: {}) {
    return this.request.post(data, 'history').pipe(
      tap((resp: User) => {
        this.userInfo = resp;
        this.header.loggedIn.next(true);
      }));
  }
}
